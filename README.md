# HIFIS Corporate Design
These guidelines present the visual identity of HIFIS and are applicable to all HIFIS-related services.

## Table of Content
[TOC]

## Logo
The default HIFIS logo is:

![HIFIS logo](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/HIFIS/HIFIS_blue/HIFIS_blue.svg){width=25%}

It is to be used on light background and always with the cloud. 
Prefer using the `.svg` format whenever possible. `.png` formats are also available in [this folder](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/tree/master/logos/HIFIS):
- 24dpi to be used for icons
- 96dpi to be used for normal screen resolution
- 300dpi to be used for printed material

The cloud alone can be used as a favicon or in derivative logos but the text part cannot be used alone.

### Favicons
Square versions of the symbol can be used as favicons. Different formats are available [in this folder](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/tree/master/logos/HIFIS/HIFIS_symbol_blue) if the `.svg` is not handled.

![HIFIS favicon](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/HIFIS/HIFIS_symbol_blue/HIFIS_symbol_blue_24x24.png)

### Logo Variations
On darker backgrounds, the logo can be used in its white variations:
- [HIFIS logo white](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/tree/master/logos/HIFIS/HIFIS_white)
- [HIFIS symbol white](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/tree/master/logos/HIFIS/HIFIS_symbol_white)


### Derivatives for HIFIS Cervices
HIFIS-related services or groups have derivatives logos:
- Helmholtz Cloud [:link:](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/tree/master/logos/Helmholtz_Cloud)

![](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/Helmholtz_Cloud/logo_blue.svg){height=70px}

- Helmholtz ID [:link:](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/Helmholtz_ID)

![](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/Helmholtz_ID/logo_blue.svg){height=70px}

- Helmholtz Trusted Network [:link:](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/Helmholtz_Trusted_Network)

![](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/Helmholtz_Trusted_Network/logo_blue.svg){height=70px}

White variations can also be used on darker backgrounds. They can be found in the linked folders.

:mailbox: Please [add an issue here](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/issues) to request a new spin-off.

### Acknowledgment
#### Web-based Services
To acknowledge HIFIS for a web-based service, add the following sentence in your footer, along with the HIFIS logo:
> `Service Name` is being developed and maintained by the Helmholtz-wide HIFIS platform. To learn more about HIFIS, visit [hifis.net](https://www.hifis.net/). If you need help or want to contact us, you can write an email to [support@hifis.net](mailto:support@hifis.net). 

For example:
![](assets/Helmholtz-cloud-footer-ex.png)

#### Publications
To acknowledge HIFIS in a publication, please inkluce the following text:
> We gratefully acknowledge the HIFIS (Helmholtz Federated IT Services) team for … (e.g. support with the components above).

## Color Palette
### Primary Color
The primary color to be used is the Helmholtz dark blue: `#002864`.

### Secondary Colors
The following secondary colors from the [Helmholtz branding guidelines](https://syncandshare.desy.de/index.php/f/581280613) can also be used, by order of preference:
- white `#ffffff`
- web pale blue `#ecfbfd`
- light blue `#14c8ff`
- highlight blue `#cdeefb`
- claim green `#008040`
- mint `#05e5ba`

### Color Usage
- __Titles__ should be either in dark blue `#002864`, light blue `#14c8ff` or in white `#ffffff`.
- __Texts__ should be either in black `#000000` or in grey `#444444` for larger text sections.
- __Backgrounds__ should be either in white `#ffffff`, web pale blue `#ecfbfd` or `#f8f8f8` as background for longer text sections.
- Other colors can be used for buttons, highlights and other graphical elements.

## Typography
The preferred font for HIFIS is the [__Inter__](https://codebase.helmholtz.cloud/hifis/overall/cdm/fonts/-/tree/main/Inter) typeface.

## Language, Capital Letters and Spelling of HIFIS Services
### Language
Since summer 2024, use **American English**.   
(When reviewing older websites etc., replace British English.) 
### Capital Letters
Use capital letters in headlines and titles, e. g. *Helmholtz Cloud Sets Quality Criteria for its Services*

- Pronounce important words; basically all words, except
    - Articles (a, an, the)
    - Coordinating Conjunctions (and, but, for)
    - Short (less than 5 letters) Prepositions (at, by, from)
- See, e.g. [here](https://headlinecapitalization.com/) and [here](https://www.enago.com/academy/best-practices-for-using-headline-case/).

### HIFIS Service - Spelling
#### Dos:
* Helmholtz Cloud | Helmholtz ID | Helmholtz Trusted Network
* All Processes with capital letters: Service Onboarding (process), Service Integration ...
* ...

#### Don'ts:
* Helmholtz-Cloud | Helmholtz-ID ... ** not even in German language**
* Helmholtz cloud | Helmholtz trusted network ...
* service Onboarding, service onboarding
...

## Templates
:construction:

## Icons and illustrations

### Icons
There is no HIFIS-designed set of icons, we therefore encourage you to use simple, free icons like the ones provided by [FontAwesome](https://fontawesome.com/v6/search?o=r&m=free), preferably using our color palette though. On a website, you can include these as follows:

```html
<i class="fa-solid fa-bell" style="color: #002864;"></i>
```
Otherwise they can be downloaded as `.svg` files for your offline projects. Here's a few commonly used examples:

![](assets/calendar-day-solid.svg){width=4%} &emsp;
![](assets/envelope-solid.svg){width=4%} &emsp;
![](assets/circle-info-solid.svg){width=4%} &emsp;
![](assets/list-solid.svg){width=4%} &emsp;
![](assets/location-arrow-solid.svg){width=4%} &emsp;
![](assets/tags-solid.svg){width=4%} &emsp;
![](assets/up-right-from-square-solid.svg){width=4%} &emsp;
![](assets/magnifying-glass-solid.svg){width=4%} &emsp;
![](assets/user-group-solid.svg){width=4%} &emsp;


### Illustrations
Some illustrations were designed for HIFIS by Lorna Schütte and can be re-used with acknowledgement to the author. These can be found [in this folder](https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/tree/master/illustrations).

Additionally, we recommend the use of the great set of open-source illustrations created by Katerina Limpitsouni: [__unDraw__](https://undraw.co/illustrations). These should be personalised on-the-fly with the HIFIS color palette.

A small selection of the huge set is [stored here](https://codebase.helmholtz.cloud/hifis/overall/hifis.net/-/tree/master/assets/img/illustrations) for hifis.net. 

One example:

![](https://codebase.helmholtz.cloud/hifis/overall/hifis.net/-/raw/master/assets/img/illustrations/undraw_team_collaboration_re_ow29.svg){width=60%}

## Links and Buttons
:construction:
### Sign in With Helmholtz ID Button
By default, the `sign in with Helmholtz ID` button should be defined as follows:
```html
<button style="
    background: #002864;
    color: white;
    border-radius: 4px;
    height: 40px;
    padding: 0 30px;
    display: flex;
    align-items: center;
    justify-content: center;
">
    <img src="https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/HIFIS/HIFIS_symbol_white/HIFIS_symbol_white.svg" style="
        margin-right: 10px;
        height: 60%;
    ">
    <span style="font-family: 'Inter', sans-serif;">Sign in with Helmholtz ID</span>
</button>
```
Which would render into something like:

![](assets/sign-in-button-ex.png)

To adjust to your page's design, you can change:
- the font and its weight and size,
- the button corner radius and shadow, to match the other buttons you have,
- the width of the button to your page's design.

#### White Variation
A white style is also available and can be defined as follows:

```html
<button style="
    background: white;
    color: #002864;
    border-radius: 4px;
    height: 40px;
    padding: 0 30px;
    display: flex;
    align-items: center;
    justify-content: center;
">
    <img src="https://codebase.helmholtz.cloud/hifis/overall/cdm/templates/-/raw/master/logos/HIFIS/HIFIS_symbol_blue/HIFIS_symbol_blue.svg" style="
        margin-right: 10px;
        height: 60%;
    ">
    <span style="font-family: 'Inter', sans-serif;">Sign in with Helmholtz ID</span>
</button>
```
Which would render into something like:

![](assets/sign-in-button-white-ex.png)

#### Short Version
In case "Sign in with" shouldn't appear in the button itself, e.g. because there are several login methods at the same level, you can use a short version of the button containing only the text "Helmholtz ID".

:construction:

## Accessibility
:construction:

## Licences
Please refer to the original projects for the licence information.